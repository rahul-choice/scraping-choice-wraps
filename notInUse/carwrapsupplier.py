# import requests
# from bs4 import BeautifulSoup
# import re
# import csv
# from selenium import webdriver
#
# with open('carwrapsupplier.csv', 'w', newline='') as file:
#     writer = csv.writer(file)
#     writer.writerow(["Title", "Content", "Preview Image", "Product Link", "Images"])
#
# browser = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")
#
# links = [
#     'https://carwrapsupplier.com/product-category/platinum-series/',
#     'https://carwrapsupplier.com/product-category/new-colors/',
#     'https://carwrapsupplier.com/product-category/ultra-gloss/',
#     'https://carwrapsupplier.com/product-category/carbon-fiber/',
#     'https://carwrapsupplier.com/product-category/gloss-vinyl-wrap/',
#     'https://carwrapsupplier.com/product-category/gloss-metallic-2/',
#     'https://carwrapsupplier.com/product-category/color-shift-2/',
#     'https://carwrapsupplier.com/product-category/matte-vinyl-wrap/',
#     'https://carwrapsupplier.com/product-category/satin/',
#     'https://carwrapsupplier.com/product-category/metallic-sparkle/',
#     'https://carwrapsupplier.com/product-category/indestructible/',
#     'https://carwrapsupplier.com/product-category/holographic/',
#     'https://carwrapsupplier.com/product-category/camouflage/',
#     'https://carwrapsupplier.com/product-category/brushed-aluminum/',
#     'https://carwrapsupplier.com/product-category/wood-grain-vinyl/',
#     'https://carwrapsupplier.com/product-category/marble-2/',
#     'https://carwrapsupplier.com/product-category/snake-skin/',
#     'https://carwrapsupplier.com/product-category/velvet/',
#     'https://carwrapsupplier.com/product-category/glow-in-the-dark/',
#     'https://carwrapsupplier.com/product-category/chrome-vinyl-wrap/'
#     'https://carwrapsupplier.com/product-category/vvivid-premium/',
#     'https://carwrapsupplier.com/product-category/vvivid-xpo/'
# ]
#
# for link in links:
#     resp = requests.get(
#         url=link
#     )
#     scope = str(resp.content)
#     pages = 1
#     if not scope.__contains__("showing all"):
#         pages = re.findall(
#             '<p class="woocommerce-result-count">(.*?)<nav class="woocommerce-pagination">',
#             str(scope)
#         )[0]
#
#     print(pages)
#
# for i in range(1, 2):
#     resp = requests.get(url='https://www.cheetahwrap.com/cheetahwrap-vinyl-all-colors/')
#     scope = str(resp.content)
#     product_names = [p.split("60&quot; ")[1] for p in re.findall("<div class=\"title\" style=\"color:(.*?)</div>", str(scope))]
#     product_preview_links = re.findall("<img class=\"card-image lazyload\" data-sizes=\"auto\" src=\"https://cdn11.bigcommerce.com/s-5oyrrbtvve/stencil/16d4f520-6d95-013a-90ef-726c04bee930/e/08595fc0-d050-0138-7011-0242ac11000e/img/loading.svg\" data-src=\"(.*?)\" alt=\"", str(scope))
#     product_links = ["https://www.cheetahwrap.com/6" + p for p in re.findall("<a href=\"https://www.cheetahwrap.com/6(.*?)\">", str(scope))]
#
#     for idx, l in enumerate(product_links):
#
#         resp = requests.get(
#             url=l
#         )
#
#         scope = str(resp.content)
#
#         product_images = re.findall(
#             "<img data-sizes=\"auto\" src=\"(.*?)\" alt=\"",
#             str(scope)
#         )
#
#         product_description = re.findall(
#             "<div class=\"tab-content is-active\" id=\"tab-description\">(.*?)<p><a href=\"https://www.cheetahwrap.com/customer-reviews\">",
#             str(scope)
#         )[0]
#
#         with open('carwrapsupplier.csv', 'a', newline='') as file:
#             writer = csv.writer(file)
#             writer.writerow([product_names[idx], product_description, product_preview_links[idx], l, product_images])
#
#         print(product_images)
