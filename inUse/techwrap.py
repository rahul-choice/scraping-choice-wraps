import requests
from bs4 import BeautifulSoup
import re
import csv

with open('../out/techwrap.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(["Title", "Content", "Preview Image", "Product Link", "Images"])


links = [
    'https://teckwrap.com/collections/gloss-vinyl',
    'https://teckwrap.com/collections/matte-vinyl',
    'https://teckwrap.com/collections/chrome-vinyl',
    'https://teckwrap.com/collections/color-shift-vinyl',
    'https://teckwrap.com/collections/basic-structure-vinyl',
    'https://teckwrap.com/collections/premium-structure-vinyl'
]

for link in links:

    resp = requests.get(
        url=link
    )

    scope = str(resp.content)

    products = [p.replace("\\r", "").replace("\\t", "").replace("\\n", "").strip() for p in re.findall('<h3 class="product-item__title h3  underline-animation ">(.*?)</h3>',str(scope))]
    product_links = ['https://teckwrap.com' + p.split('<a href="')[1] for p in re.findall('<div class="product-item ">(.*?)" >',str(scope))]

    for idx, pl in enumerate(product_links):
        resp = requests.get(url=pl)
        scope = str(resp.content)
        product_images = ['https://cdn.shopify.com/s/files' + p for p in re.findall(' 480w, //cdn.shopify.com/s/files(.*?) 720w, //cdn.shopify', str(scope))]
        description = [p for p in re.findall('<meta name="twitter:description" content="(.*?)">', str(scope))][0]

        with open('../out/techwrap.csv', 'a', newline='') as file:
            writer = csv.writer(file)
            writer.writerow([products[idx], description, product_images[0], pl, product_images])
