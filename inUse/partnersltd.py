import requests
from bs4 import BeautifulSoup
import re
import csv

with open('partners-ltd.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(["Title", "Content", "Preview Image", "Product Link", "Images"])

records = []

for i in range(1, 2):
    is_forbidden = True
    while (is_forbidden):
        resp = requests.get(url='https://partners-ltd.com/en/product-cat/3m-car-wrap-films/')
        scope = str(resp.content)
        is_forbidden = scope.__contains__("Forbidden")

    product_names = re.findall("<h4 class=\"ui--title-text\" style=\"font-weight: bold; text-align: left;\">(.*?)</h4>", str(scope))
    product_links = ['https://partners-ltd.com/en/shop/' + p.split('"')[0] for p in re.findall('<a href="https://partners-ltd.com/en/shop/(.*?)" style="text-decoration: none;" class="tooltips">', str(scope).split("Sign Box Calculator")[1])]
    preview_image_links = [re.findall("data-srcset=\"(.*?) 100w, ", p) for p in re.findall("<img width=\"100\" height=\"100\" src=\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns%3D&#039;http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg&#039; viewBox%3D&#039;0 0 100 100&#039;%2F%3E\" class=\"attachment-thumbnail size-thumbnail lazy-load preload-me wp-post-image\" (.*?)/> </a>", str(scope))]

    for idx, l in enumerate(product_links):
        is_forbidden = True
        try_count = 0
        skip_product = False
        while (is_forbidden):
            resp = requests.get(
                url=l
            )
            scope = str(resp.text)
            is_forbidden = scope.__contains__("Forbidden")
            try_count +=1
            if (try_count >= 10):
                skip_product = True
                break

        if skip_product:
            print("skipped : " + l)
            continue



        product_images = re.findall(
            'data-thumb="(.*?)" data-thumb-alt="',
            str(scope)
        )

        product_name = re.findall(
            '<meta property="og:title" content="(.*?)" />',
            str(scope)
        )[0]

        try:
            product_description = str(re.findall(
                '<div class="woocommerce-product-details__short-description">(.*?)<table class="md-cstyl">',
                str(str(resp.content))
            )[0]).replace("<p>", "").replace("</p>", "")
        except:
            print("skipped : " + l)
            continue

        with open('partners-ltd.csv', 'a', newline='') as file:
            writer = csv.writer(file)
            writer.writerow([product_name, product_description, product_images[0], l, product_images])
