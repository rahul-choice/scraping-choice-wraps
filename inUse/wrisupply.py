import requests
from bs4 import BeautifulSoup
import re
import csv

with open('../out/wrapfilms-wrisupply-com.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(["Title", "Content", "Preview Image", "Product Link", "Images"])

records = []

resp = requests.get(url='https://wrapfilms.wrisupply.com/app/themes/wri-supply/api/products?filter=&page=all&offset=12&quickShip=0&order=')
data = resp.json()
progress = 0

for record in data['data']['hits']:

    print(str((progress/len(data['data']['hits']))*100) + "%" + " Complete")
    progress+=1

    title = record['_source']['short_pattern_name']
    content = record['_source']['post_content']
    image_tile = record['_source']['image']
    slug = record['_source']['image']
    permalink = record['_source']['permalink']
    page = requests.get(url=permalink)
    soup = BeautifulSoup(page.content, "html.parser")
    images = []

    for div in soup.findAll('div', {'class': 'woocommerce-product-gallery__image'}):
        image = re.search(r"<a href=\"(.*?)\"", str(div)).group(0).replace("<a href=\"","").replace("\"", "")
        images.append(image)

    with open('../out/wrapfilms-wrisupply-com.csv', 'a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([title, content.replace("\n", "\\n").replace("\r", "\\r"), image_tile, permalink, images])
