import requests
from bs4 import BeautifulSoup
import re
import csv

with open('../out/cheetah-wrap.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(["Title", "Content", "Preview Image", "Product Link", "Images"])

records = []

for i in range(1, 2):
    resp = requests.get(url='https://www.cheetahwrap.com/cheetahwrap-vinyl-all-colors/')
    scope = str(resp.content)
    product_names = [p.split("60&quot; ")[1] for p in re.findall("<div class=\"title\" style=\"color:(.*?)</div>", str(scope))]
    product_preview_links = re.findall("<img class=\"card-image lazyload\" data-sizes=\"auto\" src=\"https://cdn11.bigcommerce.com/s-5oyrrbtvve/stencil/16d4f520-6d95-013a-90ef-726c04bee930/e/08595fc0-d050-0138-7011-0242ac11000e/img/loading.svg\" data-src=\"(.*?)\" alt=\"", str(scope))
    product_links = ["https://www.cheetahwrap.com/6" + p for p in re.findall("<a href=\"https://www.cheetahwrap.com/6(.*?)\">", str(scope))]

    for idx, l in enumerate(product_links):

        resp = requests.get(
            url=l
        )

        scope = str(resp.content)

        product_images = re.findall(
            "<img data-sizes=\"auto\" src=\"(.*?)\" alt=\"",
            str(scope)
        )

        product_description = re.findall(
            "<div class=\"tab-content is-active\" id=\"tab-description\">(.*?)<p><a href=\"https://www.cheetahwrap.com/customer-reviews\">",
            str(scope)
        )[0]

        with open('../out/cheetah-wrap.csv', 'a', newline='') as file:
            writer = csv.writer(file)
            writer.writerow([product_names[idx], product_description, product_preview_links[idx], l, product_images])

        print(product_images)
